const express = require('express')
const routers = express.Router()
const ctrl = require('../controllers/product')
const upload = require('../libs/upload')

routers.get('/', ctrl.GetProdutcs)
routers.post('/', upload.single('image'), ctrl.AddData)
routers.put('/:prod_id', ctrl.Update)

module.exports = routers
