const ctrl = {}
const model = require('../models/product')
const logss = require('../libs/logger')(module)

ctrl.GetProdutcs = async (req, res) => {
    try {
        const data = await model.GetAll()
        return res.status(200).send(data)
    } catch (error) {
        logss.error(error)
        return res.status(500).send(error)
    }
}

ctrl.AddData = async (req, res) => {
    try {
        const { name, price, qty } = req.body
        const images = 'http://localhost:8080/' + req.file.path
        const data = await model.Save({ name, price, qty, images })
        return res.status(200).send(data)
    } catch (error) {
        logss.error(error)
        return res.status(500).send(error)
    }
}

ctrl.Update = async (req, res) => {
    try {
        const { prod_id } = req.params
        const data = await model.update(prod_id, req.body)
        return res.status(200).send(data)
    } catch (error) {
        logss.error(error)
        return res.status(500).send(error)
    }
}

module.exports = ctrl
