const db = require('../config/db')
const { DataTypes, Model } = require('sequelize')
const userDetail = require('./userDetails')
class Users extends Model {}

Users.init(
    {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        username: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false
        }
    },
    { sequelize: db, timestamps: true, modelName: 'Users' }
)

Users.hasOne(userDetail, {
    foreignKey: 'userId',
    as: 'userDetail',
    onDelete: 'CASCADE'
})

module.exports = Users
